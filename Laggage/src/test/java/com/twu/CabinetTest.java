package com.twu;

import exception.InvalidTicked;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

public class CabinetTest {
    Cobinet cobinet;

    @BeforeEach
    void setUp() {
        cobinet = new Cobinet();
    }

    @Test
    public void should_return_ticket_when_deposit_laggage() {
        Ticket ticket = cobinet.doposit(new Laggage());
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_laggage_when_use_ticket() {
        Laggage myLaggage = new Laggage();
        Ticket ticket = cobinet.doposit(myLaggage);

        Laggage laggage = cobinet.claim(ticket);
        Assertions.assertEquals(myLaggage, laggage);

    }


    @Test
    void should_return_laggage_when_have_two_lagguge() {
        Laggage myLaggage = new Laggage();
        Ticket ticket = cobinet.doposit(myLaggage);
        cobinet.doposit(new Laggage());

        Laggage laggage = cobinet.claim(ticket);
        Assertions.assertEquals(myLaggage, laggage);

    }

    @Test
    void should_no_deposit() {
        Laggage myLaggage = new Laggage();
        Ticket ticket = cobinet.doposit(null);

        Laggage laggage = cobinet.claim(ticket);
        Assertions.assertNull(laggage);
    }

    @Test
    void should_throw_false_ticket() {
        Ticket trueTicket = cobinet.doposit(new Laggage());
        Assertions.assertThrows(InvalidTicked.class, () -> {
            cobinet.claim(new Ticket());
        });
    }

    @Test
    void should_throw_exception_when_claim_twice() {
        Laggage laggage = new Laggage();
        Ticket ticket = cobinet.doposit(laggage);
        cobinet.claim(ticket);
        Assertions.assertThrows(InvalidTicked.class, () -> {
            cobinet.claim(ticket);
        });
    }

    @Test
    void should_throw_expection_when_capacity_is_too_small() {
        IntStream.range(0, 3).forEach(i -> cobinet.doposit(new Laggage()));
        Assertions.assertThrows(NoMoreSpaceExpection.class,()->{
            cobinet.doposit(new Laggage());
        });
    }
}
