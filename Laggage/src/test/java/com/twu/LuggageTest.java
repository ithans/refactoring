package com.twu;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

public class LuggageTest {

    @ParameterizedTest
    @EnumSource(
            value = LuggageSize.class,
            names={"ExtraLarge","Large","Medium","Small","Minin"}
    )
    void should_return_luggage_size(LuggageSize size){
        Laggage laggage = new Laggage(size);
        Assertions.assertEquals(size,laggage.getLuggageSize());
    }

}

