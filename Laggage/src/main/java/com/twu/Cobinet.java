package com.twu;

import exception.InvalidTicked;

import java.util.HashMap;
import java.util.Map;

public class Cobinet {
    private Laggage laggage;
    private int capacity;
    private Map<Ticket, Laggage> locker = new HashMap<>();

    public Cobinet(int capacity) {
        this.capacity = 3;
    }

    public Cobinet() {
        this(3);
    }

    public Ticket doposit(Laggage laggage) {
        if (locker.size()==3){
            throw new NoMoreSpaceExpection();
        }
        Ticket ticket = new Ticket();
        locker.put(ticket, laggage);
        this.laggage = laggage;
        return ticket;
    }

    public Laggage claim(Ticket ticket) {
        if (!(locker.containsKey(ticket))){
            throw new InvalidTicked();
        }
       return locker.remove(ticket);

    }
}
