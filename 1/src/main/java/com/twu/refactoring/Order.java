package com.twu.refactoring;

import java.util.List;

public class Order {
    String name;
    String address;
    List<LineItem> list;

    public Order(String name, String address, List<LineItem> list) {
        this.name = name;
        this.address = address;
        this.list = list;
    }

    public String getCustomerName() {
        return name;
    }

    public String getCustomerAddress() {
        return address;
    }

    public List<LineItem> getLineItems() {
        return list;
    }

    public static String printReceipt(Order order) {
        StringBuilder output = new StringBuilder();

        output.append(order.getCustomerName());
        output.append(order.getCustomerAddress());
        double totSalesTx = 0d;
        double tot = 0d;
        for (LineItem lineItem : order.getLineItems()) {
            output.append(lineItem.getDescription());
            output.append('\t');
            output.append(lineItem.getPrice());
            output.append('\t');
            output.append(lineItem.getQuantity());
            output.append('\t');
            output.append(lineItem.totalAmount());
            output.append('\n');


            double salesTax = lineItem.totalAmount() * 0.1;
            totSalesTx += salesTax;
            tot += lineItem.totalAmount() + salesTax;
        }

        output.append("Sales Tax").append('\t').append(totSalesTx);

        output.append("Total Amount").append('\t').append(tot);
        return output.toString();
    }
}
